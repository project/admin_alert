This module was created mainly for distribution maintenance.  It does only two
things:

1. Alters administrative emails (new account created, etc.) so that they are
   sent to all users who have administrative roles, instead of to the site email
   or to the email of user 1.
2. Provides a system for notifying users with administrative roles when new
   features are added, bugs are fixed, etc.

